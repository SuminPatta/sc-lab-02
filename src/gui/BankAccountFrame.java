package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
A frame that shows the growth of an investment with variable interest.
*/

public class BankAccountFrame extends JFrame {
	
	private static final int FRAME_WIDTH = 450;
	   private static final int FRAME_HEIGHT = 100;

	   private static final double DEFAULT_RATE = 5;

	   private JLabel rateLabel;
	   private JTextField rateField;
	   private JButton button;
	   private JLabel resultLabel;
	   private JPanel panel;
	   public BankAccountFrame(JLabel resultLabel)
	   {
		  this.resultLabel = resultLabel;
	      createTextField(DEFAULT_RATE);
	      createButton();
	      createPanel();
	      
	      setVisible(true);
	      setSize(FRAME_WIDTH, FRAME_HEIGHT);
	   }
	   
	   public JLabel getResultLabel(){
		   
		   return resultLabel;
	   }
	   
	   public JTextField getRateField() {
		   return rateField;
	   }
	   
	   public JButton getButton(){
		   return button;
	   }
	   
	   private void createTextField(double rate)
	   {
	      rateLabel = new JLabel("Interest Rate: ");

	      final int FIELD_WIDTH = 10;
	      rateField = new JTextField(FIELD_WIDTH);
	      rateField.setText("" + rate);
	   }
	   
	   private void createButton()
	   {	button = new JButton("Add Interest");
	   }
	   
	   public void createPanel()
	   {
	      panel = new JPanel();
	      panel.add(rateLabel);
	      panel.add(getRateField());
	      panel.add(button);
	      panel.add(resultLabel);
	      add(panel);
	   }

	
	} 


