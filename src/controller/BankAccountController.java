package controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import gui.BankAccountFrame;
import model.BankAccount;


/*�����֧���͡��� object �ͧ BankAccount ��� InvestmentFrame �� attribute / local variable � class controller 
>>>> ���Шз���� class controller ����ö���¡��ҹ���ʹ�ͧ����ͧclass�� �·�� class�ͧBankAccount���class �ͧInvestmentFrame
����ͧ�������ǡѹ ���class controller ��class�Ǻ�����÷ӧҹ�ͧ�����*/


public class BankAccountController {
	
	private BankAccountFrame frame;
	private BankAccount account;
	private JLabel resultLabel;
	private AddInterestListener listener;
	
	public static void main(String[] args){
		new BankAccountController();
	}

	public BankAccountController(){
		
		account = new BankAccount(1000);
		resultLabel = new JLabel("balance: " + account.getBalance());
		frame = new BankAccountFrame(resultLabel);
		listener = new AddInterestListener();
		frame.getButton().addActionListener(listener);

	}

	class AddInterestListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			double rate = Double.parseDouble(frame.getRateField().getText());
			double interest = account.getBalance() * rate / 100;
			account.deposit(interest);
			frame.getResultLabel().setText("balance: " + account.getBalance());
		}            
	}

}
